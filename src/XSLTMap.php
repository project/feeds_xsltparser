<?php

namespace Drupal\feeds_xsltparser;

/**
 * Apply a map of XSLTPipes keyed by integers or strings.
 */
class XSLTMap implements XSLTPipeInterface {

  /**
   * Constructs an map of xslt pipes.
   *
   * @param \Iterator $map
   *   An associative array of key => pipe.
   * @param string $xsltParamKeyName
   *   Name of the xslt parameter to pass the key to.
   * @param ?\Drupal\feeds_xsltparser\XSLTPipeInterface $preproc
   *   An optional preprocessor pipe.
   */
  public function __construct(
    private \Iterator $map,
    private string $xsltParamKeyName = '_item',
    private ?XSLTPipeInterface $preproc = NULL,
  ) {
    $this->map = $map;
    $this->xsltParamKeyName = $xsltParamKeyName;
    $this->preproc = $preproc;
  }

  /**
   * Sets the xslt parameter key name.
   *
   * @param string $xsltParamKeyName
   *   Name of the xslt parameter to pass the key to.
   */
  public function setXsltParamKeyName(string $xsltParamKeyName): void {
    $this->xsltParamKeyName = $xsltParamKeyName;
  }

  /**
   * Sets the preprocessor.
   *
   * @param \Drupal\feeds_xsltparser\XSLTPipeInterface $preproc
   *   The preprocessor pipe.
   */
  public function setPreprocessor(XSLTPipeInterface $preproc) {
    $this->preproc = $preproc;
  }

  /**
   * {@inheritdoc}
   */
  public function run(string $markup, $params = []) {
    // Run preprocessor pipe if configured.
    if ($this->preproc) {
      $markup = $this->preproc->run($markup, $params);
    }

    // If the mapping function itself is implemented as an XSLTPipeInterface,
    // like e.g. XSLTRepeat, run it here on the input.
    if ($this->map instanceof XSLTPipeInterface) {
      $this->map->run($markup, $params);
    }

    $result = [];
    foreach ($this->map as $key => $pipe) {
      $pipe_params = $params + [$this->xsltParamKeyName => $key];
      $result[$key] = $pipe->run($markup, $pipe_params);
    }

    return $result;
  }

}
