<?php

namespace Drupal\feeds_xsltparser\Feeds\Parser\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;
use Drupal\feeds_xsltparser\XSLTPipeRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The configuration form for the XSLT pipeline parser.
 */
class XsltPipelineParserForm extends ExternalPluginFormBase implements ContainerInjectionInterface {

  /**
   * Constructs new configuration form for the XSLT pipeline parser.
   *
   * @param string $appRoot
   *   Path to the application root directory.
   */
  public function __construct(protected string $appRoot) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $appRoot = $container->getParameter('app.root');
    assert(is_string($appRoot));
    return new static($appRoot);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->plugin->getConfiguration();

    $form['xslt_dir'] = [
      '#type' => 'textfield',
      '#title' => $this->t('XSLT Pipelines Path'),
      '#description' => $this->t('A path on the server to the directory where your XSLT pipelines are stored'),
      '#size' => 60,
      '#default_value' => $configuration['xslt_dir'] ?? '',
    ];

    $xslt_dir = $this->configGetXsltDirectory();
    if ($xslt_dir === FALSE) {
      $fieldmap = FALSE;
      $fieldLabels = [];
      $count = 0;
    }
    else {
      $repository = new XSLTPipeRepository($xslt_dir);
      $config = $repository->config();
      $fieldmap = $repository->extractFieldmap($config);
      $fieldLabels = array_keys($fieldmap['mapper']['items']);
      $count = count($fieldLabels);
    }

    if ($count === 0 || $fieldmap === FALSE) {
      $form['xslt_help'] = [
        '#type' => 'markup',
        '#markup' => $this->t('No pipelines found in XSLT pipeline path.') . '<br/>' .
        $this->t('In order to create pipelines upload your XSLT files into the specified directory. If you place folders containing XSLT files under the XSLT pipeline path, the stylesheets will be executed in alphabetic order, each operating on the result of its predecessor.') . '<br/>' .
        $this->t('Each pipeline is exposed as a source field in the mapping section'),
      ];
    }
    else {
      $items = $fieldmap['mapper']['items'];
      $pipes = [];
      foreach ($fieldLabels as $label) {
        $pipes[$label] = $this->describeFieldExtractor($items[$label], $xslt_dir);
      }

      $form['xslt_help'] = [
        '#theme' => 'feeds_xsltparser_pipelist',
        '#pipes' => $pipes,
      ];
    }

    return $form;
  }

  /**
   * Return a description for field extractor.
   *
   * Returns a string describing the given field extractor fragment from a
   * pipeline config. Typically you use this to generate help text. See
   * FeedsXSLTPipelineParser:configForm for an example of how this function
   * is called.
   */
  private function describeFieldExtractor($item, string $base_dir) {
    $reloffset = mb_strlen($base_dir) + 1;

    $output = $this->t('Unknown extractor type');
    if ($item['type'] == 'XSLTMap' && $item['mapper']['type'] == 'XSLTRepeat') {
      $extractors = substr_replace(
        $item['mapper']['pipe']['stylesheets'],
        '',
        0,
        $reloffset
      );
      $counters = substr_replace(
        $item['mapper']['counter']['stylesheets'],
        '',
        0,
        $reloffset
      );
      $output = $this->t('Extract multiple field values by applying @extracttext %extractors repeatedly. The XSLT parameter %paramname is set to the current iteration (0 in the first pass, 1 in the second, etc.). The number of repetitions is determined using @countertext %counters, which must return an integer value.', [
        '@extracttext' => $this->formatPlural(count($extractors), 'stylesheet', 'stylesheet chain comprised of the extractors'),
        '%paramname' => $item['paramKey'],
        '%extractors' => implode(', ', $extractors),
        '@countertext' => $this->formatPlural(count($counters), 'stylesheet', 'stylesheet chain comprised of the counters'),
        '%counters' => implode(', ', $counters),
      ]);

      if (isset($item['preproc'])) {
        $preprocs = substr_replace($item['preproc']['stylesheets'], '', 0, $reloffset);
        $output .= ' ';
        $output .= $this->t('Input gets preprocessed using @filetext %files, counting and value extraction is done on the result obtained form the preprocessor.', [
          '@filetext' => $this->formatPlural(count($preprocs), 'stylesheet', 'stylesheet chain comprised of the files'),
          '%files' => implode(', ', $preprocs),
        ]);
      }
    }
    elseif ($item['type'] == 'XSLTChain') {
      $extractors = substr_replace($item['stylesheets'], '', 0, $reloffset);
      $output = $this->t('Extract one field value using @filetext %files.', [
        '@filetext' => $this->formatPlural(count($extractors), 'stylesheet', 'stylesheet chain comprised of the files'),
        '%files' => implode(', ', $extractors),
      ]);
    }

    return $output;
  }

  /**
   * Return the absolute path to the configured xslt directory if it exists.
   *
   * @return string|false
   *   An absolute path to the configured directory or FALSE.
   */
  private function configGetXsltDirectory(): string|false {
    $result = FALSE;

    $configuration = $this->plugin->getConfiguration();
    if (isset($configuration['xslt_dir']) && is_string($configuration['xslt_dir'])) {
      $xslt_dir = $configuration['xslt_dir'];
      if ($xslt_dir[0] != '/') {
        $xslt_dir = $this->appRoot . '/' . $xslt_dir;
      }
      if (is_dir($xslt_dir)) {
        $result = $xslt_dir;
      }
    }

    return $result;
  }

}
