<?php

namespace Drupal\feeds_xsltparser;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\feeds\StateInterface;

/**
 * Default implementation of xslt chain log proxy.
 *
 * An implementation of XSLTCHainLogProxy which writes every libxml warning and
 * error to the feeds importer log. The first error-level message is retained
 * in order to give exception handlers a clue what happened inside the XSLTPipe
 * structure beeing run.
 */
class FeedsXSLTChainLogProxy implements XSLTChainLogProxyInterface {
  use StringTranslationTrait;

  /**
   * Original libxml error.
   */
  private ?\LibXMLError $originalError;

  /**
   * Original libxml prefix.
   */
  private ?string $originalPrefix;

  /**
   * Original xslt path.
   */
  private ?string $originalXslt;

  /**
   * Constructs new xlt chain log proxy.
   *
   * @param \Drupal\feeds\StateInterface $state
   *   The feeds state.
   * @param string $configPrefix
   *   The xslt config directory.
   */
  public function __construct(
    private StateInterface $state,
    private string $configPrefix,
  ) {
    $this->state = $state;
    $this->configPrefix = $configPrefix;
  }

  /**
   * {@inheritdoc}
   */
  public function log(\LibXMLError $xmlError, string $prefix, ?string $xsltPath = NULL) {
    $isError = $xmlError->level > LIBXML_ERR_WARNING;

    if ($isError && !$this->originalError) {
      $this->originalError = $xmlError;
      $this->originalPrefix = $prefix;
      $this->originalXslt = $xsltPath;
    }

    $this->state->setMessage(
      $this->formatXmlError($xmlError, $prefix, $xsltPath),
      $isError ? 'warning' : 'error',
      TRUE
    );
  }

  /**
   * Format an XML error.
   */
  public function formatXmlError(\LibXMLError $xmlError, string $prefix, ?string $xsltPath = NULL) {
    $differentPrefix = substr_compare($this->configPrefix, $xsltPath, 0,
      mb_strlen($this->configPrefix));
    if (!$differentPrefix) {
      $xsltPath = mb_substr($xsltPath, mb_strlen($this->configPrefix));
    }

    $differentPrefix = substr_compare($this->configPrefix, $xmlError->file, 0,
      mb_strlen($this->configPrefix));
    if (!$differentPrefix) {
      $xmlPath = mb_substr($xmlError->file, mb_strlen($this->configPrefix));
    }
    else {
      $xmlPath = $xmlError->file;
    }

    $msg = $this->t('Message: %message<br/>Chain: %prefix<br/>File: %path Line: %line Column: %col',
      [
        '%message' => $xmlError->message,
        '%prefix' => $prefix,
        '%path' => $xmlPath,
        '%line' => $xmlError->line,
        '%col' => $xmlError->column,
      ]);
    if ($xsltPath) {
      $msg .= '<br/>' . $this->t('XSLT file: %path', ['%path' => $xsltPath]);
    }

    return $msg;
  }

  /**
   * Reset original error.
   */
  public function resetOriginalError() {
    $this->originalError = NULL;
    $this->originalPrefix = NULL;
    $this->originalXslt = NULL;
  }

  /**
   * Return original error and prefix.
   */
  public function getOriginalError() {
    return [$this->originalError, $this->originalPrefix, $this->originalXslt];
  }

}
