<?php

namespace Drupal\feeds_xsltparser;

/**
 * Interface for XSLT pipeline log proxy classes.
 */
interface XSLTChainLogProxyInterface {

  /**
   * Log a libxml message.
   *
   * @param \LibXMLError $xmlError
   *   The libxml error or warning.
   * @param string $prefix
   *   The xslt config directory.
   * @param ?string $xsltPath
   *   The path to the affected xslt if any.
   */
  public function log(\LibXMLError $xmlError, string $prefix, ?string $xsltPath);

}
