<?php

// phpcs:disable Drupal.Strings.UnnecessaryStringConcat.Found

namespace Drupal\Tests\feeds_xsltparser\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\feeds_xsltparser\XSLTChain;
use Drupal\feeds_xsltparser\XSLTMap;

/**
 * @coversDefaultClass \Drupal\feeds_xsltparser\XSLTMap
 * @group feeds_xsltparser
 */
class XSLTMapTest extends UnitTestCase {

  /**
   * Whether XSLT is available on this host.
   */
  private bool $xslAvailable;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->xslAvailable = extension_loaded('xsl');

    if (!$this->xslAvailable) {
      trigger_error('XSL Extension is not available on this system. Skipping this test.', E_USER_NOTICE);
      return;
    }
  }

  /**
   * Tests a map of stylesheets.
   */
  public function testMapRun() {
    if (!$this->xslAvailable) {
      return;
    }

    $first_xslt =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' .
      '<xsl:output method="text" encoding="UTF-8"/>' .
      '<xsl:template match="input">' .
      'simple' .
      '</xsl:template>' .
      '</xsl:stylesheet>';
    $first_proc = new \XSLTProcessor();
    $doc = new \DOMDocument();
    $doc->loadXML($first_xslt);
    $first_proc->importStylesheet($doc);
    $first_chain = new XSLTChain();
    $first_chain->addProcessor($first_proc);

    $second_xslt =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' .
      '<xsl:output method="text" encoding="UTF-8"/>' .
      '<xsl:param name="_item"/>' .
      '<xsl:template match="input">' .
      'Hello <xsl:value-of select="$_item"/>' .
      '</xsl:template>' .
      '</xsl:stylesheet>';
    $second_proc = new \XSLTProcessor();
    $doc = new \DOMDocument();
    $doc->loadXML($second_xslt);
    $second_proc->importStylesheet($doc);
    $second_chain = new XSLTChain();
    $second_chain->addProcessor($second_proc);

    $mapper = new \ArrayIterator([
      'field1' => $first_chain,
      'field2' => $second_chain,
    ]);

    $map = new XSLTMap($mapper, '_item');
    $result = $map->run('<input/>');

    $this->assertTrue(is_array($result), 'Result of XSLTMap::run() must be an array');
    $this->assertEquals(2, count($result), 'Result must have the same number of elements as the map');
    $this->assertEquals(['field1', 'field2'], array_keys($result), 'Result array must have the same keys as the map');
    $this->assertEquals('simple', $result['field1']);
    $this->assertEquals('Hello field2', $result['field2']);
  }

  /**
   * Tests a map of stylesheets with preprocessor.
   */
  public function testMapWithPreproc() {
    if (!$this->xslAvailable) {
      return;
    }

    $preproc_xslt =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' .
      '<xsl:output method="xml" encoding="UTF-8"/>' .
      '<xsl:template match="/">' .
      '<preprocessed/>' .
      '</xsl:template>' .
      '</xsl:stylesheet>';
    $preproc_proc = new \XSLTProcessor();
    $doc = new \DOMDocument();
    $doc->loadXML($preproc_xslt);
    $preproc_proc->importStylesheet($doc);
    $preproc_chain = new XSLTChain();
    $preproc_chain->addProcessor($preproc_proc);

    $first_xslt =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' .
      '<xsl:output method="text" encoding="UTF-8"/>' .
      '<xsl:template match="preprocessed">' .
      'perfect' .
      '</xsl:template>' .
      '</xsl:stylesheet>';
    $first_proc = new \XSLTProcessor();
    $doc = new \DOMDocument();
    $doc->loadXML($first_xslt);
    $first_proc->importStylesheet($doc);
    $first_chain = new XSLTChain();
    $first_chain->addProcessor($first_proc);

    $second_xslt =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' .
      '<xsl:output method="text" encoding="UTF-8"/>' .
      '<xsl:template match="preprocessed">' .
      'success' .
      '</xsl:template>' .
      '</xsl:stylesheet>';
    $second_proc = new \XSLTProcessor();
    $doc = new \DOMDocument();
    $doc->loadXML($second_xslt);
    $second_proc->importStylesheet($doc);
    $second_chain = new XSLTChain();
    $second_chain->addProcessor($second_proc);

    $mapper = new \ArrayIterator([
      'field1' => $first_chain,
      'field2' => $second_chain,
    ]);

    $map = new XSLTMap($mapper, '_item', $preproc_chain);
    $result = $map->run('<ignored/>');

    $this->assertEquals(['field1' => 'perfect', 'field2' => 'success'], $result, 'Preprocessor stylesheet must be applied to all chains.');
  }

}
