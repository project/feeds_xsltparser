<?php

// phpcs:disable Drupal.Strings.UnnecessaryStringConcat.Found

namespace Drupal\Tests\feeds_xsltparser\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\feeds_xsltparser\XSLTChain;
use Drupal\feeds_xsltparser\XSLTRepeat;

/**
 * @coversDefaultClass \Drupal\feeds_xsltparser\XSLTRepeat
 * @group feeds_xsltparser
 */
class XSLTRepeatTest extends UnitTestCase {

  /**
   * Whether XSLT is available on this host.
   */
  private bool $xslAvailable;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->xslAvailable = extension_loaded('xsl');

    if (!$this->xslAvailable) {
      trigger_error('XSL Extension is not available on this system. Skipping this test.', E_USER_NOTICE);
      return;
    }
  }

  /**
   * Ensure that repeater repeats any given stylesheet 3 times.
   */
  public function testRepeat() {
    if (!$this->xslAvailable) {
      return;
    }

    $counterxslt =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' .
      '<xsl:output method="text" encoding="UTF-8"/>' .
      '<xsl:template match="/">' .
      '3' .
      '</xsl:template>' .
      '</xsl:stylesheet>';
    $counterproc = new \XSLTProcessor();
    $doc = new \DOMDocument();
    $doc->loadXML($counterxslt);
    $counterproc->importStylesheet($doc);

    $counter = new XSLTChain();
    $counter->addProcessor($counterproc);

    $chain = new XSLTChain();
    $mapper = new XSLTRepeat($counter, $chain);

    $mapper->run('<anything/>');

    $count = 0;
    foreach ($mapper as $index => $repeated) {
      $this->assertEquals($chain, $repeated, 'Repeater must repeat the given repeated n times');
      $count++;
    }

    $this->assertEquals(3, $count);
  }

  /**
   * Test that repeater respects if counter stylesheet returns zero.
   */
  public function testRepeatZero() {
    if (!$this->xslAvailable) {
      return;
    }

    // Counter stylesheet: just return '0' on any input.
    $counterxslt =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' .
      '<xsl:output method="text" encoding="UTF-8"/>' .
      '<xsl:template match="/">' .
      '0' .
      '</xsl:template>' .
      '</xsl:stylesheet>';
    $zeroproc = new \XSLTProcessor();
    $doc = new \DOMDocument();
    $doc->loadXML($counterxslt);
    $zeroproc->importStylesheet($doc);

    $counter = new XSLTChain();
    $counter->addProcessor($zeroproc);

    $chain = new XSLTChain();
    $mapper = new XSLTRepeat($counter, $chain);

    $mapper->run('<anything/>');

    foreach ($mapper as $index => $repeated) {
      $this->fail('Repeater must not repeat the stylesheet if the result of applying the counter stylesheet was zero');
    }
  }

  /**
   * Test that repeater respects if counter stylesheet returns invalid value.
   */
  public function testRepeatMustFailIfStylesheetOutputIsInvalid() {
    if (!$this->xslAvailable) {
      return;
    }

    // Nonumber stylesheet: just return '3' on any input.
    $nonumberxslt =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' .
      '<xsl:output method="text" encoding="UTF-8"/>' .
      '<xsl:template match="/">' .
      'this is not a number!' .
      '</xsl:template>' .
      '</xsl:stylesheet>';
    $nonumberproc = new \XSLTProcessor();
    $doc = new \DOMDocument();
    $doc->loadXML($nonumberxslt);
    $nonumberproc->importStylesheet($doc);

    $counter = new XSLTChain();
    $counter->addProcessor($nonumberproc);

    $chain = new XSLTChain();
    $mapper = new XSLTRepeat($counter, $chain);

    $this->expectException(\Exception::class);
    $mapper->run('<anything/>');
  }

}
