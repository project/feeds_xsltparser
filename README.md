# Feeds XSLT Pipeline Parser

This module provides a Feeds parser plugin implementing simple XSLT pipelines.
XSLT is especially useful when importing document-style XML files.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/feeds_xsltparser).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/feeds_xsltparser)

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following modules:

- [Feeds](https://www.drupal.org/project/feeds)

This module requires the following PHP extensions:

- [PHP XSL extension](http://php.net/manual/book.xsl.php)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

After installing and enabling the Feeds XSLT Pipeline Parser as well as the
Feeds module, follow these steps to setup an XSLT importer:

1. Navigate to "Structure" > "Feeds types" and click on the tab "Add feed type"
1. Setup basic settings and fetcher according to your needs
1. Change the parser to "XSLT Pipeline Parser"
1. Upload your XSLT stylesheets to sites/all/libraries/xslt_pipelines, one for
   each field you want to extract from the original document. You also may
   place folders containing XSLT files under the XSLT pipelines path. In this
   case the stylesheets will be executed in alphabetic order, each operating on
   the result of its precedessor.
1. You may change the XSLT pipelines path in the parser settings. This is
   especially useful if you want to setup multiple importers based on Feeds
   XSLT Pipeline Parser and want to use a different set of stylesheets for each
   of them.
1. Finally setup the mapping in the processor section

Refer to the feeds documentation for detailed instructions on how to setup and
customize importers.

Maintainers
-----------

This module was initially implemented by s_leu at MD Systems and sponsored by
the swiss weekly "Die Wochenzeitung".

Current maintainers:
- [znerol](https://www.drupal.org/u/znerol)
