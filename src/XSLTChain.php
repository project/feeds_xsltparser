<?php

namespace Drupal\feeds_xsltparser;

/**
 * Implementation of an XSLT pipe.
 *
 * A simple XSLT pipeline applying a set of XSLT stylesheets to the given
 * markup one after the other.
 */
class XSLTChain implements XSLTPipeInterface {

  /**
   * List of XSLT processor instances.
   *
   * @var \XSLTProcessor[]
   */
  private $xsltProcessors = [];

  /**
   * List of paths to xslt stylesheets.
   *
   * @var array<string|null>
   */
  private $xsltPaths = [];

  /**
   * Last state returned by libxml_use_internal_errors().
   */
  private bool $xmlLastRecordErrors = FALSE;

  /**
   * A log proxy instance.
   */
  private ?XSLTChainLogProxyInterface $logproxy = NULL;

  /**
   * A prefix for logged messages.
   */
  private ?string $logprefix;

  /**
   * {@inheritdoc}
   */
  public function run($markup, $params = []) {
    foreach ($this->xsltProcessors as $i => $proc) {
      // Parse markup.
      $this->beginRecordLibMessages();
      $doc = new \DOMDocument();
      $doc->loadXML($markup);
      $ok = $this->endRecordLibMessages();

      if ($doc === FALSE || $ok === FALSE) {
        throw new LibXMLException('Failed to load XML while running XSLT chain.');
      }

      // Run stylesheets.
      $this->beginRecordLibMessages();
      $proc->setParameter('', $params);
      $markup = $proc->transformToXML($doc);
      $ok = $this->endRecordLibMessages($this->xsltPaths[$i]);

      if ($markup === FALSE || $ok === FALSE) {
        throw new LibXMLException('Failed to transform XML using XSLT chain.');
      }
    }

    return $markup;
  }

  /**
   * Sets the log proxy instance.
   *
   * @param \Drupal\feeds_xsltparser\XSLTChainLogProxyInterface $logproxy
   *   A log proxy instance.
   * @param ?string $logprefix
   *   The log prefix.
   */
  public function setLogProxy(XSLTChainLogProxyInterface $logproxy, ?string $logprefix = NULL): void {
    $this->logproxy = $logproxy;
    $this->logprefix = $logprefix;
  }

  /**
   * Add an xslt processor.
   *
   * @param \XSLTProcessor $proc
   *   An XSLT processor instance.
   * @param ?string $path
   *   The path to the XSLT stylesheet.
   */
  public function addProcessor(\XSLTProcessor $proc, ?string $path = NULL): void {
    $this->xsltProcessors[] = $proc;
    $this->xsltPaths[] = $path;
  }

  /**
   * Add an xslt processor from the filesystem.
   *
   * @param string $path
   *   The path to the XSLT processor.
   */
  public function addProcessorFromPath(string $path): void {
    // Loading a malformed XML document results in a fatal error
    // (LIBXML_ERR_FATAL)
    $this->beginRecordLibMessages();
    $doc = new \DOMDocument();
    $doc->load($path, LIBXML_NOCDATA);
    $ok = $this->endRecordLibMessages();

    if ($doc === FALSE || $ok === FALSE) {
      throw new LibXMLException('Failed to load XML file at ' . $path);
    }

    // According to the PHP sourcecode, XSLTProcessor::importStylesheet does
    // return TRUE on success and FALSE on failure at least since 5.2.0.
    // Therefore I rely on this undocumented feature here.
    // Attempting to import an XSLT from an arbritary but well formed XML file
    // results in multiple LIBXML_ERR_ERROR.
    $proc = new \XSLTProcessor();
    $this->beginRecordLibMessages();
    $result = $proc->importStylesheet($doc);
    $ok = $this->endRecordLibMessages();

    if ($result === FALSE || $ok === FALSE) {
      throw new LibXMLException('Failed to load XSLT stylesheet from ' . $path);
    }

    $this->addProcessor($proc, $path);
  }

  /**
   * Turns on error recording.
   *
   * Turns error recording on for until it is turned off using
   * endRecordLibMessages again.
   */
  private function beginRecordLibMessages(): void {
    $this->xmlLastRecordErrors = libxml_use_internal_errors(TRUE);
  }

  /**
   * Turns off error recording.
   *
   * Returns false if the recorded messages contain an LibXMLError whose level
   * is above LIBXML_ERR_WARNING, otherwise returns true.
   */
  private function endRecordLibMessages($xsltPath = NULL) {
    $result = TRUE;
    foreach (libxml_get_errors() as $xmlMessage) {
      if ($xmlMessage->level > LIBXML_ERR_WARNING) {
        $result = FALSE;
      }
      if ($this->logproxy) {
        $this->logproxy->log($xmlMessage, $this->logprefix, $xsltPath);
      }
    }

    libxml_clear_errors();
    libxml_use_internal_errors($this->xmlLastRecordErrors);

    return $result;
  }

}
