<?php

namespace Drupal\feeds_xsltparser;

/**
 * Exception class for XML exceptions.
 */
class LibXMLException extends \Exception {
}
