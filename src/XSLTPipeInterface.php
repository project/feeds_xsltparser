<?php

namespace Drupal\feeds_xsltparser;

/**
 * Interface for XSLT pipeline classes.
 */
interface XSLTPipeInterface {

  /**
   * Run the XSLT pipeline.
   *
   * @param string $markup
   *   The markup to operate on.
   * @param array $params
   *   Params to pass to the XSLT stylesheet(s).
   *
   * @return string|array
   *   The result after applying the XSLT stylesheet(s).
   */
  public function run(string $markup, array $params = []);

}
