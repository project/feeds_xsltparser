<?php

namespace Drupal\feeds_xsltparser\Feeds\Parser;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Feeds\Item\DynamicItem;
use Drupal\feeds\Feeds\Parser\ParserBase;
use Drupal\feeds\Result\FetcherResultInterface;
use Drupal\feeds\Result\ParserResult;
use Drupal\feeds\StateInterface;
use Drupal\feeds_xsltparser\FeedsXSLTChainLogProxy;
use Drupal\feeds_xsltparser\XSLTPipeRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a XSLT pipeline feed parser.
 *
 * @FeedsParser(
 *   id = "xslt_pipeline",
 *   title = "XSLT Pipeline Parser",
 *   description = @Translation("A simple XSLT pipeline parser for feeds."),
 *   form = {
 *     "configuration" = "Drupal\feeds_xsltparser\Feeds\Parser\Form\XsltPipelineParserForm",
 *   },
 * )
 */
class XSLTPipelineParser extends ParserBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs new configuration form for the XSLT pipeline parser.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param string $appRoot
   *   Path to the application root directory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected string $appRoot) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $appRoot = $container->getParameter('app.root');
    assert(is_string($appRoot));
    return new static($configuration, $plugin_id, $plugin_definition, $appRoot);
  }

  /**
   * Return the absolute path to the configured xslt directory if it exists.
   *
   * @return string|false
   *   An absolute path to the configured directory or FALSE.
   */
  private function configGetXsltDirectory(): string|false {
    $result = FALSE;

    $xslt_dir = $this->configuration['xslt_dir'];
    if ($xslt_dir[0] != '/') {
      $xslt_dir = $this->appRoot . '/' . $xslt_dir;
    }
    if (is_dir($xslt_dir)) {
      $result = $xslt_dir;
    }
    else {
      $this->messenger()->addWarning(t('The XSLT pipeline directory does not exist. Please check your feed settings.'));
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function parse(FeedInterface $feed, FetcherResultInterface $fetcher_result, StateInterface $state) {
    $xslt_dir = $this->configGetXsltDirectory();
    if ($xslt_dir === FALSE) {
      $message = $this->t('The XSLT pipeline directory does not exist. Please check your feed settings.');
      $state->setMessage($message, 'error');
      throw new \Exception($message);
    }

    $repository = new XSLTPipeRepository($xslt_dir);
    $config = $repository->config();

    $markup = $fetcher_result->getRaw();

    $logproxy = new FeedsXSLTChainLogProxy($state, $xslt_dir . '/');
    /** @var \Drupal\feeds\Feeds\Item\ItemInterface[] */
    $items = [];
    try {
      $pipe = $repository->load($config, $logproxy);
      foreach ($pipe->run($markup) as $row) {
        $items[] = (new DynamicItem())->fromArray($row);
      }
    }
    catch (\Exception $e) {
      $message = $e->getMessage();
      [$xmlError, $xmlPrefix, $xsltPath] = $logproxy->getOriginalError();

      if ($xmlError) {
        $message .= '<br/>';
        $message .= $logproxy->formatXmlError($xmlError, $xmlPrefix, $xsltPath);
      }

      $state->setMessage($message, 'error');
      throw new \Exception($message);
    }

    return (new ParserResult())->addItems($items);
  }

  /**
   * Return the list of XSLT pipelines found in the pipelines directory.
   */
  public function getMappingSources() {
    $xslt_dir = $this->configGetXsltDirectory();
    if ($xslt_dir === FALSE) {
      return [];
    }

    $repository = new XSLTPipeRepository($xslt_dir);
    $config = $repository->config();
    $fieldmap = $repository->extractFieldmap($config);

    $fieldLabels = array_keys($fieldmap['mapper']['items']);

    $map = [];
    foreach ($fieldLabels as $label) {
      $map[$label] = [
        'label' => $label,
        'description' => t('A field value extracted using the @label XSLT pipeline.', ['@label' => $label]),
      ];
    }

    return $map;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'xslt_dir' => 'sites/all/libraries/xslt_pipelines',
    ];
  }

}
