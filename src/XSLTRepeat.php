<?php

namespace Drupal\feeds_xsltparser;

/**
 * Implements an index iterator over one XSLTPipe.
 *
 * The number of times the pipe is repeated is determined by applying the
 * counter pattern.
 */
class XSLTRepeat implements \Iterator, XSLTPipeInterface {
  /**
   * The current index.
   */
  private int $idx = 0;

  /**
   * Number of repetitions.
   */
  private int $repetitions = 0;

  /**
   * Construct an XSLTRepeat instance.
   *
   * @param \Drupal\feeds_xsltparser\XSLTChain $counter
   *   An XSLTChain returning an integer denoting the number of
   *   repetitions.
   * @param \Drupal\feeds_xsltparser\XSLTPipeInterface $pipe
   *   The XSLTPipe which should be repeated.
   */
  public function __construct(private XSLTChain $counter, private XSLTPipeInterface $pipe) {
    $this->counter = $counter;
    $this->pipe = $pipe;
  }

  /**
   * {@inheritdoc}
   */
  public function current(): mixed {
    return $this->pipe;
  }

  /**
   * {@inheritdoc}
   */
  public function key(): mixed {
    return $this->idx;
  }

  /**
   * {@inheritdoc}
   */
  public function next(): void {
    $this->idx++;
  }

  /**
   * {@inheritdoc}
   */
  public function rewind(): void {
    $this->idx = 0;
  }

  /**
   * {@inheritdoc}
   */
  public function valid(): bool {
    return $this->idx < $this->repetitions;
  }

  /**
   * {@inheritdoc}
   */
  public function run($markup, $params = []) {
    $result = trim($this->counter->run($markup, $params));
    if (!preg_match('/^\d+$/', $result)) {
      throw new \Exception('Result of counter stylesheet must be a positive integer.');
    }

    $this->repetitions = intval($result);
    return $result;
  }

}
