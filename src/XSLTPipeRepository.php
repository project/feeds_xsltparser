<?php

namespace Drupal\feeds_xsltparser;

/**
 * Generate XSLTPipe instances by traversing a directory structure.
 */
class XSLTPipeRepository {

  /**
   * Constructs new XSLT pipe instance.
   *
   * @param string $path
   *   The full path to a directory on the server filesystem
   *   which contains XSLT stylesheets.
   */
  public function __construct(protected string $path) {
  }

  /**
   * Generate an XSLT pipeline configuration by inspecting the given directory.
   */
  public function config() {
    // Generate a list of all files in the first and second level of the
    // directory structure with an extension of .xsl or .xslt.
    $pathlist = glob(
      $this->path . '/{*,*/*}.{[xX][sS][lL],[xX][sS][lL][tT]}',
      GLOB_MARK | GLOB_BRACE | GLOB_NOSORT
    );
    sort($pathlist);

    $extract_stylesheets = [];
    $counter_stylesheets = [];
    $preproc_stylesheets = [];
    $global_counter = [];
    $global_preproc = [];

    // Phase 1: Loop thru all files and assign them to the proper stylesheets
    // chain.
    foreach ($pathlist as $abspath) {
      $relpath = mb_substr($abspath, mb_strlen($this->path) + 1);

      // Silently ignore directories whose names happened to match the glob.
      if (mb_substr($relpath, -1) == '/') {
        continue;
      }

      // Derive the label from path.
      $matchcount = preg_match('/^_?([0-9]+[_-]?)?[a-z][a-z0-9_]*/i', $relpath, $matches);
      if ($matchcount != 1) {
        \Drupal::messenger()->addWarning(t('Files and directories in the first level of the XSLT pipeline directory must begin with a character. Please rename %path appropriately, otherwise the path is ignored.', ['%path' => $relpath]));
        continue;
      }

      $label = reset($matches);
      $label = preg_replace('/^(_?)([0-9]+[_-]?)?([a-z].*)$/i', '\\1\\3', $label);

      // Handle global magic chains.
      if ('_count' == $label) {
        $global_counter[] = $abspath;
        continue;
      }
      elseif ('_preproc' == $label) {
        $global_preproc[] = $abspath;
        continue;
      }
      elseif ($label[0] == '_') {
        \Drupal::messenger()->addWarning(t('Pipes beginning with an underscore are reserved for internal use, therefore %label was ignored. Currently only the special pipes <_preproc</em> and <em>_count</em> are supported.', ['%label' => $label]));
        continue;
      }

      // Handle per chain counters and preprocessors.
      if ('_count' == mb_substr($label, -6)) {
        $label = mb_substr($label, 0, -6);
        $counter_stylesheets[$label][] = $abspath;
      }
      elseif ('_preproc' == mb_substr($label, -8)) {
        $label = mb_substr($label, 0, -8);
        $preproc_stylesheets[$label][] = $abspath;
      }
      // All other stylesheets are assigned to an extract-chain.
      else {
        $extract_stylesheets[$label][] = $abspath;
      }
    }

    // Phase 2: generate the XSLT pipe configuration.
    // Build up the pipe- and chain-configurations used to extract field values.
    $field_extractors = [];
    foreach ($extract_stylesheets as $label => $paths) {
      $chain = [
        'type' => 'XSLTChain',
        'stylesheets' => $paths,
      ];

      $pipe = [];
      if (isset($counter_stylesheets[$label])) {
        $pipe['type'] = 'XSLTMap';
        $pipe['mapper'] = [
          'type' => 'XSLTRepeat',
          'counter' => [
            'type' => 'XSLTChain',
            'stylesheets' => $counter_stylesheets[$label],
          ],
          'pipe' => $chain,
        ];
        $pipe['paramKey'] = '_element';

        if (isset($preproc_stylesheets[$label])) {
          $pipe['preproc'] = [
            'type' => 'XSLTChain',
            'stylesheets' => $preproc_stylesheets[$label],
          ];
        }
      }
      else {
        $pipe = $chain;
      }

      $field_extractors[$label] = $pipe;
    }

    // Wrap the field extractors into an XSLTMap with static mapping.
    $field_mapper = [
      'type' => 'XSLTMap',
      'mapper' => [
        'type' => 'ArrayIterator',
        'items' => $field_extractors,
      ],
      'paramKey' => '_field',
    ];

    // Wrap the field mapper into toplevel XSLTMap with the global counter
    // and preprocessor.
    if (!empty($global_counter)) {
      $mapper = [
        'type' => 'XSLTRepeat',
        'counter' => [
          'type' => 'XSLTChain',
          'stylesheets' => $global_counter,
        ],
        'pipe' => $field_mapper,
      ];
    }
    else {
      $mapper = [
        'type' => 'ArrayIterator',
        'items' => [0 => $field_mapper],
      ];
    }

    $outer_map = [
      'type' => 'XSLTMap',
      'mapper' => $mapper,
      'paramKey' => '_pass',
    ];

    if (!empty($global_preproc)) {
      $outer_map['preproc'] = [
        'type' => 'XSLTChain',
        'stylesheets' => $global_preproc,
      ];
    }

    return $outer_map;
  }

  /**
   * Extract the fieldmap.
   *
   * Find the XSLTMap responsible for the field mapping in the config structure
   * by looking for an array containing 'paramKey' => '_field'.
   *
   * @return array<string, mixed[]>|false
   *   The fieldmap.
   */
  public function extractFieldmap($config): array|false {
    if (is_array($config)) {
      if (
        isset($config['type']) && $config['type'] == 'XSLTMap' &&
        isset($config['paramKey']) && $config['paramKey'] == '_field'
      ) {
        return $config;
      }

      foreach ($config as $value) {
        $result = $this->extractFieldmap($value);
        if (is_array($result)) {
          return $result;
        }
      }
    }

    return FALSE;
  }

  /**
   * Construct a pipe using the given configuration.
   */
  public function load($config, $logproxy, $logprefix = '') {
    switch ($config['type']) {
      case 'XSLTMap':
        $mapfunc = $this->load($config['mapper'], $logproxy, $logprefix);
        $mapobj = new XSLTMap($mapfunc);

        if (isset($config['paramKey'])) {
          $mapobj->setXsltParamKeyName($config['paramKey']);
        }
        if (isset($config['preproc'])) {
          $preproc = $this->load($config['preproc'], $logproxy, $logprefix . '_preproc');
          $mapobj->setPreprocessor($preproc);
        }

        $result = $mapobj;
        break;

      case 'XSLTChain':
        $chainobj = new XSLTChain();
        $chainobj->setLogProxy($logproxy, $logprefix);
        $paths = $config['stylesheets'];
        foreach ($paths as $path) {
          $chainobj->addProcessorFromPath($path);
        }

        $result = $chainobj;
        break;

      case 'XSLTRepeat':
        $counter = $this->load($config['counter'], $logproxy, $logprefix . '_counter');
        $pipe = $this->load($config['pipe'], $logproxy, $logprefix);
        $repobj = new XSLTRepeat($counter, $pipe);

        $result = $repobj;
        break;

      case 'ArrayIterator':
        $items = [];
        foreach ($config['items'] as $itemkey => $itemconfig) {
          $item = $this->load($itemconfig, $logproxy, $logprefix . '[' . $itemkey . ']');
          $items[$itemkey] = $item;
        }
        $result = new \ArrayIterator($items);
        break;

      default:
        $result = NULL;
        break;
    }

    return $result;
  }

}
