<?php

// phpcs:disable Drupal.Strings.UnnecessaryStringConcat.Found

namespace Drupal\Tests\feeds_xsltparser\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\feeds_xsltparser\LibXMLException;
use Drupal\feeds_xsltparser\XSLTChain;

/**
 * @coversDefaultClass \Drupal\feeds_xsltparser\XSLTChain
 * @group feeds_xsltparser
 */
class XSLTChainTest extends UnitTestCase {

  /**
   * XSLT processor producing text.
   */
  private \XSLTProcessor $goodtotextproc;

  /**
   * XSLT processor producing XML.
   */
  private \XSLTProcessor $goodtoxmlproc;

  /**
   * XSLT processor accepting params.
   */
  private \XSLTProcessor $goodparamproc;

  /**
   * Failing XSLT processor.
   */
  private \XSLTProcessor $badproc;

  /**
   * Valid markup.
   */
  private string $goodmarkup;

  /**
   * Invalid markup.
   */
  private string $badmarkup;

  /**
   * Whether XSLT is available on this host.
   */
  private bool $xslAvailable;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->xslAvailable = extension_loaded('xsl');

    if (!$this->xslAvailable) {
      trigger_error('XSL Extension is not available on this system. Skipping this test.', E_USER_NOTICE);
      return;
    }

    $goodxsltoxml =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' .
      '<xsl:output method="xml" encoding="UTF-8"/>' .
      '<xsl:template match="/">' .
      '  <alsogood><xsl:value-of select="."/></alsogood>' .
      '</xsl:template>' .
      '</xsl:stylesheet>';
    $this->goodtoxmlproc = new \XSLTProcessor();
    $doc = new \DOMDocument();
    $doc->loadXML($goodxsltoxml);
    $this->goodtoxmlproc->importStylesheet($doc);

    $goodxsltotext =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' .
      '<xsl:output method="text" encoding="UTF-8"/>' .
      '<xsl:template match="/">' .
      '<xsl:value-of select="."/>' .
      '</xsl:template>' .
      '</xsl:stylesheet>';
    $this->goodtotextproc = new \XSLTProcessor();
    $doc = new \DOMDocument();
    $doc->loadXML($goodxsltotext);
    $this->goodtotextproc->importStylesheet($doc);

    $paramxslt =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' .
      '<xsl:output method="text" encoding="UTF-8"/>' .
      '<xsl:param name="testparam"/>' .
      '<xsl:template match="/">' .
      '<xsl:value-of select="$testparam"/>' .
      '</xsl:template>' .
      '</xsl:stylesheet>';
    $this->goodparamproc = new \XSLTProcessor();
    $doc = new \DOMDocument();
    $doc->loadXML($paramxslt);
    $this->goodparamproc->importStylesheet($doc);

    $badxslt =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' .
      '<xsl:output method="text" encoding="UTF-8"/>' .
      '<xsl:template match="/">' .
      '<xsl:message terminate="yes">Simulate runtime error</xsl:message>' .
      '</xsl:template>' .
      '</xsl:stylesheet>';
    $this->badproc = new \XSLTProcessor();
    $doc = new \DOMDocument();
    $doc->loadXML($badxslt);
    $this->badproc->importStylesheet($doc);

    $this->goodmarkup =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<good>Heck, it works!</good>';

    $this->badmarkup =
      '<?xml version="1.0" encoding="UTF-8"?>' .
      '<really>not parsable</bad>';
  }

  /**
   * The original markup should be returned by the XSLTChain.
   *
   * If no processor was added to the chain.
   */
  public function testChainWithoutAnyProcessorShouldReturnOriginalMarkup() {
    if (!$this->xslAvailable) {
      return;
    }

    $chain = new XSLTChain();
    $result = $chain->run($this->goodmarkup);

    $this->assertEquals($this->goodmarkup, $result);
  }

  /**
   * Test the result of the XSLT transformation with one single stylesheet.
   */
  public function testChanWithOneStylesheetShouldReturnCorrectResult() {
    if (!$this->xslAvailable) {
      return;
    }

    $chain = new XSLTChain();
    $chain->addProcessor($this->goodtotextproc);
    $result = $chain->run($this->goodmarkup);

    $this->assertEquals('Heck, it works!', $result);
  }

  /**
   * Test the result of the XSLT transformation with two stylesheets.
   *
   * Test wether the result of the XSLT transformation with two stylesheet
   * applied one on top of the results of the other is correct.
   */
  public function testChanWithMultipleStylesheetsShouldReturnCorrectResult() {
    if (!$this->xslAvailable) {
      return;
    }

    $chain = new XSLTChain();
    $chain->addProcessor($this->goodtoxmlproc);
    $chain->addProcessor($this->goodtotextproc);
    $result = $chain->run($this->goodmarkup);

    $this->assertEquals('Heck, it works!', $result);
  }

  /**
   * Test the result of the XSLT transformation with params.
   */
  public function testChanWithParam() {
    if (!$this->xslAvailable) {
      return;
    }

    $chain = new XSLTChain();
    $chain->addProcessor($this->goodparamproc);

    $result = $chain->run($this->goodmarkup, ['testparam' => 'first']);
    $this->assertEquals('first', $result);

    $result = $chain->run($this->goodmarkup, ['testparam' => 'second']);
    $this->assertEquals('second', $result);
  }

  /**
   * Ensure that an exception is thrown when the markup is incorrect.
   */
  public function testChainExceptionOnInvalidMarkup() {
    if (!$this->xslAvailable) {
      return;
    }

    $chain = new XSLTChain();
    $chain->addProcessor($this->goodtotextproc);

    $this->expectException(LibXMLException::class);
    $chain->run($this->badmarkup);
  }

  /**
   * Test that an exception is thrown when a runtime error occures.
   */
  public function testChainExceptionOnProcRuntimeError() {
    if (!$this->xslAvailable) {
      return;
    }

    $chain = new XSLTChain();
    $chain->addProcessor($this->badproc);

    $this->expectException(LibXMLException::class);
    $chain->run($this->goodmarkup);
  }

  /**
   * Test the result of the transformation with an XSLT loaded from a path.
   */
  public function testAddProcessorFromPath() {
    if (!$this->xslAvailable) {
      return;
    }

    $path = __DIR__ . '/../../fixture/goodtotext.xslt';
    $chain = new XSLTChain();
    $chain->addProcessorFromPath($path);

    $result = $chain->run($this->goodmarkup);

    $this->assertEquals('Heck, it works!', $result);
  }

  /**
   * Test that an exception is thrown when a XML loaded from file is invalid.
   */
  public function testAddProcessorFromPathWithInvalidStylesheet() {
    if (!$this->xslAvailable) {
      return;
    }

    $path = __DIR__ . '/../../fixture/noxsl.xslt';
    $chain = new XSLTChain();

    $this->expectException(LibXMLException::class);
    $chain->addProcessorFromPath($path);
  }

  /**
   * When attempting to load a stylesheet from a path which does not exist.
   */
  public function testAddProcessorFromPathFromNonExistingPath() {
    if (!$this->xslAvailable) {
      return;
    }

    $path = __DIR__ . '/../../fixture/path-does-not-exist';
    $chain = new XSLTChain();

    $this->expectException(LibXMLException::class);
    $chain->addProcessorFromPath($path);
  }

  /**
   * Test that an exception is thrown when an XSLT loaded from file is invalid.
   */
  public function testAddProcessorFromPathWithInvalidProcFile() {
    if (!$this->xslAvailable) {
      return;
    }

    $path = __DIR__ . '/../../fixture/invalidxml.xslt';
    $chain = new XSLTChain();

    $this->expectException(LibXMLException::class);
    $chain->addProcessorFromPath($path);
  }

}
